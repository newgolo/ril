<!-- coverpage.md -->

![logo](images/logo.png)

> 一款嵌入式无线通信模组管理软件

 - 包含网络注册、连接、短信收发及Socket通信管理
 - 模组驱动使用插件化方式管理
 - 内置多种情况下的异常处理机制

[Gitee](https://gitee.com/moluo-tech/ril)
[快速入门](/quickStart.md)
